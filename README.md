
**Inventory:**

    entity Substrate {
        Key id : Integer64;
    		name : String(256);
    		desinfectionDurationInMinutes: Decimal(10,2);
    		expansionTimeInMinutes: Decimal(10,2);
    		notes: String(256);
    }

    entity Seed {
        Key id : Integer64;
    		name : String(256);
    		botanicalName: String(256);
    		code : Integer;
    		category : Association to SeedCategory;
    		price : Decimal(10,2);
    		seedsPerGram : Decimal(10,2);
    		supplier : String(256);
    }

    entity Fertilizer {
        Key id : Integer64;
    		name : String(256);
    		price: Decimal(10,2);
    		supplier : String(256);
    		description: String(256);
    }

**Recipe = Product number (with a BOM):**

    entity Recipe {
        Key id : Integer64;
    	Key	version : Integer default 1;
    		name : String(256);
    		seed : Association to Seed;
    		category : Association to RecipeCategory;
    		seedGramsPerTray : Decimal(10,2);
			substrate : Association to Substrate;
			germinationClimateZone : Association to GerminationClimateZone;
			germinationDurationInHours : Decimal(6,2);
			propagationClimateZone : Association to PropagationClimateZone;
			propagationDurationInHours : Decimal(6,2);
			hoursInSection1 : Decimal(6,2);
			hoursInSection2 : Decimal(6,2);
			hoursInSection3 : Decimal(6,2);
			remarks : String(512);
    }

**FarmSchedule = Production order**

**Start date of the production order = earliest seeding date**


**Due date of the production order = latest forecasted harvest of the farm schedule**

    entity FarmSchedule {
        Key id : Integer64;
    		name : String(256);
    		farm : Association to Farm ;
    		recipe : Association to Recipe ;
    		scheduledFrom : UTCDateTime;
    		scheduledTo : UTCDateTime;
    		audit : AuditTrail;
    		status : Association to FarmScheduleStatus;
    		batchesPerDay : Integer;
    }

    entity Batch {
        Key id : Integer64;
    		bench : Association to Bench;
    		farm : Association to Farm;
    		seed : Association to Seed;
    		recipe : Association to Recipe;
    		batchStatus : Association to BatchStatus;
    		seedingDate : UTCDateTime; GerminationChamber Lane
			germinationChamber : Association to GerminationChamber;
    		germinationLayer : Integer;
    		germinationChamberLane : Association to GerminationChamberLane; FarmHouseLane
    		farmHouse : Association to FarmHouse;
    		propagationLayer : Integer;
    		propagationLane : Integer;
    		farmHouseLane : Association to FarmHouseLane;
    		farmSchedule : Association to FarmSchedule;
    		pushSequence : String(512);
    		positionInPushSequence : Integer;
    		germination_pushSequence : String(512);
    		germination_positionInPushSequence : Integer;
    }

    entity Tray {
        Key id : Integer64;
    		uuid : String(64);
    		batch : Association to Batch;
    		positionInBench : Integer;
    		purchaseOrder : Association to PurchaseOrder;
    		weightInGram : Integer;
    		heightMean : Decimal(4,2);
    		heightVariance : Decimal(4,2);
    		heightStdDev : Decimal(4,2);
    		maxHeight: Decimal(5,2);
    		minHeight: Decimal(5,2);
    		growthAngle: Decimal(4,2);
    		noteOnDelivery : String(256);
    		labelPrinted : Integer default 0;
    		trayStatusOk : Integer default 1;
    }
